#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
from logging.handlers import RotatingFileHandler
import logging_loki

stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.INFO)

spike_logger = logging.getLogger("spike_logger")
formatter = logging.Formatter(fmt="%(asctime)s :: %(levelname)s :: %(message)s", datefmt="%Y-%m-%dT%H:%M:%S%z")
spike_logger.setLevel(logging.DEBUG)
spike_logger.addHandler(stream_handler)


spike_requester_logger = logging.getLogger("spike_cyanide_requester")
spike_requester_logger.setLevel(logging.INFO)
loki_handler = logging_loki.LokiHandler(url="http://Loki:3100/loki/api/v1/push", tags={"application": "tm_api"}, version="1")
loki_handler.setLevel(logging.INFO)
spike_requester_logger.addHandler(loki_handler)

spike_command_logger = logging.getLogger("spike_command_logger")


def init_log_file_handler(module_name: str):
    # Configure log file
    file_handler = RotatingFileHandler(filename="log/{}.log".format(module_name), maxBytes=10000000, backupCount=10, encoding="utf-8")
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(formatter)
    spike_logger.addHandler(file_handler)


def except_hook(except_type, value, traceback):
    spike_logger.critical(except_type)
    spike_logger.critical(value)
    spike_logger.critical(traceback)
