import json
import os


class TMSettings:

    @staticmethod
    def load_json():
        filename = os.environ.get("TM_SETTINGS_FILE", "settings/spike_settings.json")
        try:
            with open(filename, encoding="utf-8") as f:
                json_data = json.load(f)
        except FileNotFoundError:
            json_data = {}
        return json_data

    @staticmethod
    def get_database_user():
        json_data = TMSettings.load_json()
        return json_data.get("TM_MONGO_DB", {}).get("USER")

    @staticmethod
    def get_database_password():
        json_data = TMSettings.load_json()
        return json_data.get("TM_MONGO_DB", {}).get("PASSWORD")

    @staticmethod
    def get_database_host():
        json_data = TMSettings.load_json()
        return json_data.get("TM_MONGO_DB", {}).get("HOST")

    @staticmethod
    def get_bot_username():
        json_data = TMSettings.load_json()
        return json_data.get("TM_BOT", {}).get("USER")

    @staticmethod
    def get_bot_password():
        json_data = TMSettings.load_json()
        return json_data.get("TM_BOT", {}).get("PASSWORD")
