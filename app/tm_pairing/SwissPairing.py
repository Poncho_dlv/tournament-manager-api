#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random
from enum import IntEnum
from bson.objectid import ObjectId


class TournamentFormat(IntEnum):
    single_player = 0
    team_tournament = 1


class SwissPairing:
    @staticmethod
    def pair_round(sorted_team_list: list, round_index: int, tournament_id: str, reverse: bool = False):
        contests = []
        if reverse:
            sorted_team_list.reverse()
        while sorted_team_list:
            home_team = sorted_team_list.pop(0)
            away_team = next((team for team in sorted_team_list if team.get("_id") not in home_team.get("opponents", [])), None)
            if away_team is not None:
                sorted_team_list.remove(away_team)
                contests.append({
                    "tournament_id": ObjectId(tournament_id),
                    "round_index": round_index,
                    "table_index": 0,
                    "home_team":  {"id": home_team.get("_id")},
                    "away_team": {"id": away_team.get("_id")},
                    "public": False,
                    "lock": False,
                    "naf": True
                })
            else:
                return contests
        if reverse and len(contests) > 0:
            contests.reverse()
        table_index = 1
        for contest in contests:
            contest["table_index"] = table_index
            table_index += 1
        return contests

    @staticmethod
    def pair_first_round(team_list: list, tournament_id: str):
        random.shuffle(team_list)
        return SwissPairing.pair_round(team_list, 1, tournament_id)





