#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

import copy
from __init__ import spike_logger, init_log_file_handler, except_hook
from spike_database.Matches import Matches
from tm_databases.Tournaments import Tournaments
from tm_databases.Contests import Contests
from tm_databases.Standings import Standings
from tm_databases.Teams import Teams
from spike_model.Match import Match

team_template = {
    "points": 0,
    "win": 0,
    "draw": 0,
    "loss": 0,
    "games_played": 0,
    "touchdown_scored": 0,
    "touchdown_conceded": 0,
    "touchdown_difference": 0,
    "casualties_inflicted": 0,
    "casualties_sustained": 0,
    "casualties_difference": 0,
    "kill_inflicted": 0,
    "kill_sustained": 0,
    "coleman_waldorf": 0,
    "coleman_waldorf_2": 0,
    "head_to_head": [],
    "opponents": [],
    "contests": [],
    "coleman_waldorf_3": 0,
    "coleman_waldorf_4": 0,
}


def main():
    init_log_file_handler("update_coach_statistics")
    spike_logger.info("Star update")
    update_contests()
    update_standing()
    spike_logger.info("Finished")
    sys.exit()


def update_contests():
    db = Tournaments()
    match_db = Matches()
    tournaments = db.get_tournaments()
    contest_db = Contests()
    teams_db = Teams()

    for tournament in tournaments:
        contests = contest_db.get_contest_to_update(tournament["tournament_id"])
        team_list = teams_db.get_registered_team(tournament["tournament_id"], True)
        match_list = []

        for league in tournament.get("leagues", []):
            match_list.extend(match_db.get_matches_in_league(league.get("id"), "pc"))

        for idx, item in enumerate(match_list, 0):
            match_list[idx] = Match(item["match_data"])

        for contest in contests:
            competition_name = "Round {} Table {}".format(contest["round_index"], contest["table_index"])
            match = next((match for match in match_list if match.get_competition_name() == competition_name), None)

            if match is not None:
                home_team_data = next((team for team in team_list if team.get("_id") == contest.get("home_team", {}).get("id")), None)
                away_team_data = next((team for team in team_list if team.get("_id") == contest.get("away_team", {}).get("id")), None)
                home_score = match.get_score_home()
                home_cas = match.get_inflicted_casualties_home()
                home_kill = match.get_inflicted_dead_home()
                away_score = match.get_score_away()
                away_cas = match.get_inflicted_casualties_away()
                away_kill = match.get_inflicted_dead_away()
                date = match.get_start_datetime("%Y-%m-%d %H:%M")

                if home_team_data and away_team_data:
                    if home_team_data["id"] == match.get_team_home().get_id() and away_team_data["id"] == match.get_team_away().get_id():
                        contest_db.update_contest_result(str(contest["_id"]), date, home_score, home_cas, home_kill, away_score, away_cas, away_kill)
                    elif away_team_data["id"] == match.get_team_home().get_id() and home_team_data["id"] == match.get_team_away().get_id():
                        contest_db.update_contest_result(str(contest["_id"]), date, away_score, away_cas, away_kill, home_score, home_cas, home_kill)


def update_standing():
    db = Tournaments()
    tournaments = db.get_tournaments()
    contest_db = Contests()
    standing_db = Standings()

    for tournament in tournaments:
        standing = []
        teams_result = {}
        contests = contest_db.get_played_contest(tournament["tournament_id"])
        tie_breakers = ["points"]
        tie_breakers.extend(tournament.get("tie_breakers", []))
        win_point = tournament["win_value"]
        draw_point = tournament["draw_value"]
        loss_point = tournament["loss_value"]

        for contest in contests:
            home_team_id = contest["home_team"]["id"]
            away_team_id = contest["away_team"]["id"]

            if teams_result.get(home_team_id) is None:
                teams_result[home_team_id] = copy.deepcopy(team_template)
                teams_result[home_team_id]["_id"] = home_team_id

            if teams_result.get(away_team_id) is None:
                teams_result[away_team_id] = copy.deepcopy(team_template)
                teams_result[away_team_id]["_id"] = away_team_id

            if contest["home_team"]["score"] == contest["away_team"]["score"]:
                teams_result[home_team_id]["draw"] += 1
                teams_result[away_team_id]["draw"] += 1
            elif contest["home_team"]["score"] > contest["away_team"]["score"]:
                teams_result[home_team_id]["win"] += 1
                teams_result[away_team_id]["loss"] += 1
                teams_result[home_team_id]["head_to_head"].append(away_team_id)
            else:
                teams_result[home_team_id]["loss"] += 1
                teams_result[away_team_id]["win"] += 1
                teams_result[away_team_id]["head_to_head"].append(home_team_id)

            teams_result[home_team_id]["touchdown_scored"] += contest["home_team"]["score"]
            teams_result[home_team_id]["touchdown_conceded"] += contest["away_team"]["score"]
            teams_result[home_team_id]["casualties_inflicted"] += contest["home_team"]["casualties"]
            teams_result[home_team_id]["casualties_sustained"] += contest["away_team"]["casualties"]
            teams_result[home_team_id]["kill_inflicted"] += contest["home_team"]["kill"]
            teams_result[home_team_id]["kill_sustained"] += contest["away_team"]["kill"]
            teams_result[home_team_id]["opponents"].append(away_team_id)
            teams_result[home_team_id]["contests"].append(str(contest["_id"]))

            teams_result[away_team_id]["touchdown_scored"] += contest["away_team"]["score"]
            teams_result[away_team_id]["touchdown_conceded"] += contest["home_team"]["score"]
            teams_result[away_team_id]["casualties_inflicted"] += contest["away_team"]["casualties"]
            teams_result[away_team_id]["casualties_sustained"] += contest["home_team"]["casualties"]
            teams_result[away_team_id]["kill_inflicted"] += contest["away_team"]["kill"]
            teams_result[away_team_id]["kill_sustained"] += contest["home_team"]["kill"]
            teams_result[away_team_id]["opponents"].append(home_team_id)
            teams_result[away_team_id]["contests"].append(str(contest["_id"]))

        for key, value in teams_result.items():
            value["points"] = (value["win"] * win_point) + (value["draw"] * draw_point) + (value["loss"] * loss_point)

            value["games_played"] = value["win"] + value["draw"] + value["loss"]

            value["touchdown_difference"] = value["touchdown_scored"] - value["touchdown_conceded"]
            value["casualties_difference"] = value["casualties_inflicted"] - value["casualties_sustained"]

            value["coleman_waldorf"] = (value["casualties_inflicted"] / 2) + value["touchdown_scored"]
            value["coleman_waldorf_2"] = (value["casualties_difference"] / 2) + value["touchdown_difference"]
            value["coleman_waldorf_3"] = value["casualties_difference"] + value["touchdown_difference"]
            value["coleman_waldorf_4"] = value["casualties_inflicted"] + value["touchdown_scored"]

            standing.append(value)

        tie_breakers.reverse()
        for tb in tie_breakers:
            if tb == "head_to_head":
                # Apply H2H on sorted rank by points
                standing = sorted(standing, key=lambda i: i["points"], reverse=True)
                size = len(standing)
                for i in range(2):  # Hack for double sort, assuming that 3 coach tied by H2H are impossible to sort
                    for idx in range(1, size):
                        if standing[idx - 1]["_id"] in standing[idx].get("head_to_head", []):
                            tmp = standing[idx - 1]
                            standing[idx - 1] = standing[idx]
                            standing[idx] = tmp
            elif tb in["casualties_sustained", "touchdown_conceded"]:
                standing = sorted(standing, key=lambda i: i.get(tb, 0), reverse=False)
            else:
                standing = sorted(standing, key=lambda i: i.get(tb, 0), reverse=True)

        if len(standing) > 0:
            rank = 1
            for team in standing:
                team["rank"] = rank
                rank += 1
            standing_db.update_standing(tournament["tournament_id"], standing)

sys.excepthook = except_hook

if __name__ == "__main__":
    main()