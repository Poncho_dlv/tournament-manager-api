#!/usr/bin/env python
# -*- coding: utf-8 -*-

from tm_databases.BaseMongoDB import BaseMongoDb
from bson.objectid import ObjectId
from enum import IntEnum
from typing import List, Dict


class TeamStatus(IntEnum):
    ACTIVE = 0
    INACTIVE = 1


class Teams(BaseMongoDb):

    def register_teams(self, teams_list: List[Dict]):
        self.open_database()
        collection = self.db["teams"]
        ret = collection.insert(teams_list)
        self.close_database()
        return ret

    def register_team(self, team: Dict):
        self.open_database()
        collection = self.db["teams"]
        ret = collection.insert_one(team)
        self.close_database()
        return ret

    def unregister_all_teams(self, tournament_id: str):
        self.open_database()
        collection = self.db["teams"]
        res = collection.delete_many({"tournament_id": ObjectId(tournament_id)})
        self.close_database()
        return res

    def get_team_data(self, team_id: str):
        self.open_database()
        collection = self.db["teams"]
        data = collection.find_one({"_id": ObjectId(team_id)})
        self.close_database()
        return data

    def get_registered_team(self, tournament_id: str, active_only: bool = True):
        self.open_database()
        registered_teams = []
        collection = self.db["teams"]
        query = {"tournament_id": ObjectId(tournament_id)}
        if active_only:
            query["status"] = TeamStatus.ACTIVE
        teams = collection.find(query)
        self.close_database()
        for team in teams:
            team["_id"] = str(team["_id"])
            team["tournament_id"] = str(team["tournament_id"])
            registered_teams.append(team)
        return registered_teams

    def disable_registered_team(self, team_id: str):
        self.open_database()
        collection = self.db["teams"]
        new_value = {"$set": {"status": TeamStatus.INACTIVE}}
        modified_count = collection.update_one({"_id": ObjectId(team_id)}, new_value).modified_count
        self.close_database()
        return modified_count == 1

    def enable_registered_team(self, team_id: str):
        self.open_database()
        collection = self.db["teams"]
        new_value = {"$set": {"status": TeamStatus.ACTIVE}}
        modified_count = collection.update_one({"_id": ObjectId(team_id)}, new_value).modified_count
        self.close_database()
        return modified_count == 1
