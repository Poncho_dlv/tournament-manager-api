#!/usr/bin/env python
# -*- coding: utf-8 -*-

from tm_databases.BaseMongoDB import BaseMongoDb
from Models import TournamentCreationModel
from bson.objectid import ObjectId
from enum import IntEnum
from typing import List


class TournamentStatus(IntEnum):
    CREATED = 0
    STARTED = 1
    FINISHED = 2


class Tournaments(BaseMongoDb):

    def register_tournament(self, tournament_data: TournamentCreationModel):
        self.open_database()
        collection = self.db["tournaments"]
        tournament_data = tournament_data.dict(exclude_none=True)
        tournament_data["status"] = TournamentStatus.CREATED
        tournament_data["current_round"] = 0
        insert_id = collection.insert_one(tournament_data).inserted_id
        self.close_database()
        return insert_id

    def delete_tournament(self, tournament_id: str):
        self.open_database()
        collection = self.db["tournaments"]
        ret = collection.delete_one({"_id": ObjectId(tournament_id)})
        self.close_database()
        return ret

    def get_tournament_data(self, tournament_id: str):
        self.open_database()
        collection = self.db["tournaments"]
        data = collection.find_one({"_id": ObjectId(tournament_id)}, {"_id": 0})
        self.close_database()
        return data

    def update_tournament(self, tournament_id: str, update_values: dict):
        self.open_database()
        collection = self.db["tournaments"]
        new_value = {"$set": update_values}
        modified_count = collection.update_one({"_id": ObjectId(tournament_id)}, new_value).modified_count
        self.close_database()
        return modified_count == 1

    def get_tournaments(self, status: List[TournamentStatus] = None):
        self.open_database()
        tournaments = []
        collection = self.db["tournaments"]
        if status is None:
            status = [TournamentStatus.CREATED, TournamentStatus.STARTED]
        data = collection.find({"status": {"$in": status}})
        for tournament in data:
            tournament["tournament_id"] = str(tournament["_id"])
            tournament.pop("_id")
            tournaments.append(tournament)
        self.close_database()
        return tournaments

