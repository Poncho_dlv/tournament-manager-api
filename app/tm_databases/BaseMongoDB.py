#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pymongo
from tm_settings.settings import TMSettings


class BaseMongoDb:

    def __init__(self):
        self.client = None
        self.db = None

    def open_database(self):
        if self.client is None or self.db is None:
            db_user = TMSettings.get_database_user()
            db_password = TMSettings.get_database_password()
            db_host = TMSettings.get_database_host()

            if db_host is not None:
                if db_user is not None and db_password is not None:
                    self.client = pymongo.MongoClient("mongodb://{}:{}@{}/TM_DB".format(db_user, db_password, db_host), connect=False)
                else:
                    self.client = pymongo.MongoClient("mongodb://{}/TM_DB".format(db_host), connect=False)
                self.db = self.client["TM_DB"]

    def close_database(self):
        if self.client is not None:
            self.client.close()
            self.client = None
            self.db = None

