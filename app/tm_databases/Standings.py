#!/usr/bin/env python
# -*- coding: utf-8 -*-

from tm_databases.BaseMongoDB import BaseMongoDb
from bson.objectid import ObjectId
from enum import IntEnum


class ContestStatus(IntEnum):
    SCHEDULE = 0
    PLAYED = 1


class Standings(BaseMongoDb):
    def update_standing(self, tournament_id: str, standing: list):
        self.open_database()
        collection = self.db["standings"]
        query = {"tournament_id": ObjectId(tournament_id)}
        value = {"$set": {"tournament_id": ObjectId(tournament_id), "standing": standing}}
        collection.update_one(query, value, upsert=True)
        self.close_database()

    def get_standing(self, tournament_id: str):
        self.open_database()
        collection = self.db["standings"]
        query = {"tournament_id": ObjectId(tournament_id)}
        standing = collection.find_one(query, {"_id": 0})
        self.close_database()
        if standing is not None:
            return standing.get("standing", [])
        return []
