#!/usr/bin/env python
# -*- coding: utf-8 -*-

from tm_databases.BaseMongoDB import BaseMongoDb
from bson.objectid import ObjectId
from enum import IntEnum


class ContestStatus(IntEnum):
    SCHEDULE = 0
    PLAYED = 1


class Contests(BaseMongoDb):

    def register_contests(self, contests: list):
        self.open_database()
        collection = self.db["contests"]
        ret = collection.insert_many(contests)
        self.close_database()
        return ret

    def get_contests(self, tournament_id: str, round_index: int, public_only=True):
        contests = []
        self.open_database()
        collection = self.db["contests"]
        query = {"tournament_id": ObjectId(tournament_id), "round_index": round_index}
        if public_only:
            query["public"] = True
        data = collection.find(query).sort([("table_index", 1)])
        self.close_database()
        if data is not None:
            for contest in data:
                contests.append(contest)
        return contests

    def get_contest(self, contest_id: str):
        self.open_database()
        collection = self.db["contests"]
        query = {"_id": ObjectId(contest_id)}
        contest = collection.find_one(query)
        self.close_database()
        return contest

    def contest_exist(self, tournament_id: str, round_index: int, home_team_id: str, away_team_id: str):
        self.open_database()
        collection = self.db["contests"]
        data = collection.find_one({"tournament_id": ObjectId(tournament_id),  "round_index": round_index, "home_team.id": ObjectId(home_team_id), "away_team.id": ObjectId(away_team_id)}, {"_id": 1})
        self.close_database()
        return data is not None

    def update_contest_result(self, contest_id: str, date: str, home_score: int, home_cas: int, home_kill: int, away_score: int, away_cas: int, away_kill: int, lock: bool = False, naf: bool = True):
        self.open_database()
        collection = self.db["contests"]
        update_values = {
            "naf": naf,
            "lock": lock,
            "date": date,
            "home_team.score": home_score,
            "home_team.casualties": home_cas,
            "home_team.kill": home_kill,
            "away_team.score": away_score,
            "away_team.casualties": away_cas,
            "away_team.kill": away_kill
        }
        new_value = {"$set": update_values}
        modified_count = collection.update_one({"_id": ObjectId(contest_id), "lock": {"$ne": True}}, new_value).modified_count
        self.close_database()
        return modified_count == 1

    def publish_round(self, tournament_id: str, round_index: int):
        self.open_database()
        collection = self.db["contests"]
        new_value = {"$set": {"public": True}}
        ret = collection.update_many({"tournament_id": ObjectId(tournament_id), "round_index": round_index}, new_value)
        self.close_database()
        return ret

    def unpublish_round(self, tournament_id: str, round_index: int):
        self.open_database()
        collection = self.db["contests"]
        new_value = {"$set": {"public": False}}
        ret = collection.update_many({"tournament_id": ObjectId(tournament_id), "round_index": round_index}, new_value)
        self.close_database()
        return ret

    def delete_round(self, tournament_id: str, round_index: int):
        self.open_database()
        collection = self.db["contests"]
        ret = collection.delete_many({"tournament_id": ObjectId(tournament_id), "round_index": round_index})
        self.close_database()
        return ret

    def get_contest_to_update(self, tournament_id: str):
        self.open_database()
        collection = self.db["contests"]
        query = {"tournament_id": ObjectId(tournament_id), "$or": [{"lock": {"$ne": True}}, {"home_team.score": {"$exists": False}, "away_team.score": {"$exists": False}}]}
        contests = collection.find(query)
        self.close_database()
        return list(contests)

    def get_played_contest(self, tournament_id: str):
        self.open_database()
        collection = self.db["contests"]
        query = {"tournament_id": ObjectId(tournament_id), "home_team.score": {"$exists": True}, "away_team.score": {"$exists": True}}
        contests = collection.find(query)
        self.close_database()
        return list(contests)

    def update_teams(self, contest_id: str, home_team_id: str, away_team_id: str):
        self.open_database()
        collection = self.db["contests"]
        update_values = {
            "lock": False,
            "home_team": {"id": home_team_id},
            "away_team": {"id": away_team_id},
        }
        new_value = {"$set": update_values}
        modified_count = collection.update_one({"_id": ObjectId(contest_id)}, new_value).modified_count
        self.close_database()
        return modified_count == 1
