import os

from bson.objectid import ObjectId
from fastapi import HTTPException

from Models import TournamentCreationModel, tournament_format, round_format, platform, EditContest, TeamRegistration
from spike_database.Competitions import Competitions
from spike_database.BB2Resources import BB2Resources
from spike_requester.CyanideApi import CyanideApi
from spike_requester.SpikeAPI.CompetitionsAPI import CompetitionAPI
from spike_requester.SpikeAPI.LeaguesAPI import LeagueAPI
from spike_requester.Utilities import Utilities as RequestUtils
from tm_databases.Contests import Contests
from tm_databases.Teams import Teams, TeamStatus
from tm_databases.Tournaments import Tournaments, TournamentStatus
from tm_databases.Standings import Standings
from tm_pairing.SwissPairing import SwissPairing
from tm_settings.settings import TMSettings
from spike_user_db.Users import Users
from update_contests import update_contests, update_standing


def check_tournament_data(tournament_model: TournamentCreationModel):
    valid = True
    invalid_fields = []
    if tournament_model.name in ["", None]:
        valid = False
        invalid_fields.append("name")
    if tournament_model.format not in tournament_format:
        valid = False
        invalid_fields.append("format")
    if tournament_model.round_format not in round_format:
        valid = False
        invalid_fields.append("round_format")
    if tournament_model.platform not in platform:
        valid = False
        invalid_fields.append("platform")
    return valid, invalid_fields


def start_tournament(tournament_id: str):
    db = Tournaments()
    teams_db = Teams()
    tournament_data = db.get_tournament_data(tournament_id)
    if tournament_data is None:
        raise HTTPException(status_code=404, detail="Tournament not found.")

    if tournament_data.get("status") != TournamentStatus.CREATED:
        raise HTTPException(status_code=500, detail="Invalid tournament status.")

    if tournament_data.get("current_round") != 0:
        raise HTTPException(status_code=500, detail="Invalid round number")

    registered_teams = teams_db.get_registered_team(tournament_id)
    nb_teams = len(registered_teams)
    if nb_teams % 2 != 0:
        raise HTTPException(status_code=500, detail="Invalid number of team")

    if tournament_data.get("number_of_round", 0) == 0 or tournament_data.get("number_of_round", 0) >= nb_teams:
        raise HTTPException(status_code=500, detail="Incompatibility between number of round and number of team")

    if tournament_data.get("round_format") == "round_swiss":
        res = db.update_tournament(tournament_id, {"status": TournamentStatus.STARTED})
        if res:
            return {"tournament_id": tournament_id, "success": res}
        else:
            raise HTTPException(status_code=500, detail="Unable to start")
    else:
        raise HTTPException(status_code=500, detail="Invalid tournament format")


def close_tournament(tournament_id: str):
    db = Tournaments()
    tournament_data = db.get_tournament_data(tournament_id)
    if tournament_data is None:
        raise HTTPException(status_code=404, detail="Tournament not found.")

    if tournament_data.get("status") != TournamentStatus.STARTED:
        raise HTTPException(status_code=500, detail="Invalid tournament status.")

    if tournament_data.get("current_round") != tournament_data.get("number_of_round"):
        raise HTTPException(status_code=500, detail="Invalid tournament round.")
    ret = db.update_tournament(tournament_id, {"status": TournamentStatus.FINISHED})
    return {"tournament_id": tournament_id, "closed": ret}


def get_contest(contest_id: str):
    contests_db = Contests()
    contest = contests_db.get_contest(contest_id)
    if contest is None:
        raise HTTPException(status_code=500, detail="Invalid contest.")
    contest["_id"] = str(contest["_id"])
    contest["tournament_id"] = str(contest["tournament_id"])
    return contest


def update_contest(contest_id: str, contest_data: EditContest):
    db = Contests()
    ret = db.update_contest_result(contest_id, contest_data.date, contest_data.score_home, contest_data.cas_home, contest_data.kill_home, contest_data.score_away, contest_data.cas_away, contest_data.kill_away, contest_data.lock)
    return {"contest_id": contest_id, "success": ret}


def get_current_round(tournament_id: str):
    tournament_db = Tournaments()
    tournament_data = tournament_db.get_tournament_data(tournament_id)
    return get_contests(tournament_id, tournament_data.get("current_round", 0))


def get_contests(tournament_id: str, round_index: int, allow_draw: bool = False, public_only: bool = False):
    contests_db = Contests()
    contests = contests_db.get_contests(tournament_id, round_index, public_only)
    teams_db = Teams()
    bb_rsrc = BB2Resources()
    if len(contests) == 0 and allow_draw:
        tournament_db = Tournaments()
        tournament_data = tournament_db.get_tournament_data(tournament_id)
        standing_db = Standings()
        if tournament_data is not None:
            if tournament_data.get("status") != TournamentStatus.STARTED:
                raise HTTPException(status_code=500, detail="Tournament not started.")

            registered_teams = teams_db.get_registered_team(tournament_id)
            nb_teams = len(registered_teams)
            if nb_teams % 2 != 0:
                raise HTTPException(status_code=500, detail="Invalid number of team: {}".format(nb_teams))

            if tournament_data.get("current_round") > 0 and not all_matches_played(tournament_id, tournament_data.get("current_round")):
                raise HTTPException(status_code=500, detail="Match not played not played")

            if tournament_data.get("round_format") == "round_swiss":
                if tournament_data.get("format") == "single_player":
                    pairing_engine = SwissPairing()
                    if round_index == 1:
                        teams_db = Teams()
                        team_list = teams_db.get_registered_team(tournament_id)
                        contests = pairing_engine.pair_first_round(team_list, tournament_id)
                    else:
                        update_contests()
                        update_standing()
                        sorted_teams = []
                        standing = standing_db.get_standing(tournament_id)
                        max_rank = len(standing)
                        for team in registered_teams:
                            team_data = next((item for item in standing if team["_id"] == item["_id"]), None)
                            if team_data is not None:
                                sorting_data = {
                                    "_id": team_data["_id"],
                                    "opponents": team_data["opponents"],
                                    "rank": team_data["rank"]
                                }
                            else:
                                sorting_data = {
                                    "_id": team["_id"],
                                    "opponents": [],
                                    "rank": max_rank
                                }
                            sorted_teams.append(sorting_data)
                        sorted_teams = sorted(sorted_teams, key=lambda i: i["rank"])
                        contests = pairing_engine.pair_round(sorted_teams, round_index, tournament_id)
                        if len(contests) == 0:
                            contests = pairing_engine.pair_round(sorted_teams, round_index, tournament_id, True)
                    if len(contests) > 0:
                        contests_db.register_contests(contests)
                        tournament_db.update_tournament(tournament_id, {"current_round": round_index})
                else:
                    raise HTTPException(status_code=500, detail="invalid tournament_format: {}".format(tournament_data.get("format")))
            else:
                raise HTTPException(status_code=500, detail="invalid tournament_format: {}".format(tournament_data.get("round_format")))

    team_list = teams_db.get_registered_team(tournament_id, False)
    for contest in contests:
        contest["_id"] = str(contest["_id"])
        contest["tournament_id"] = str(contest["tournament_id"])
        home_team_data = next((team for team in team_list if team.get("_id") == contest.get("home_team", {}).get("id")), None)
        away_team_data = next((team for team in team_list if team.get("_id") == contest.get("away_team", {}).get("id")), None)
        contest["home_team_data"] = home_team_data
        contest["home_team_data"]["race"] = bb_rsrc.get_race_label(contest["home_team_data"]["race_id"])
        contest["away_team_data"] = away_team_data
        contest["away_team_data"]["race"] = bb_rsrc.get_race_label(contest["away_team_data"]["race_id"])
    return {"tournament_id": tournament_id, "contests": contests}


def delete_round(tournament_id: str, round_index: int):
    contests_db = Contests()
    tournament_db = Tournaments()
    tournament_data = tournament_db.get_tournament_data(tournament_id)
    if round_index != tournament_data.get("current_round"):
        raise HTTPException(status_code=500, detail="Enable to delete past or future round.")
    contests_db.delete_round(tournament_id, round_index)
    return {"tournament_id": tournament_id}


def publish_round(tournament_id: str, round_index: int):
    contests_db = Contests()
    tournament_db = Tournaments()
    tournament_data = tournament_db.get_tournament_data(tournament_id)
    if round_index != tournament_data.get("current_round"):
        raise HTTPException(status_code=500, detail="Enable to publish past or future round.")
    contests_db.publish_round(tournament_id, round_index)
    return {"tournament_id": tournament_id}


def unpublish_round(tournament_id: str, round_index: int):
    contests_db = Contests()
    tournament_db = Tournaments()
    tournament_data = tournament_db.get_tournament_data(tournament_id)
    if round_index != tournament_data.get("current_round"):
        raise HTTPException(status_code=500, detail="Enable to unpublish past or future round.")
    contests_db.unpublish_round(tournament_id, round_index)
    return {"tournament_id": tournament_id}


def register_teams(tournament_id: str):
    platform_id = 1
    teams_db = Teams()
    user_db = Users()
    tournament_db = Tournaments()
    tournament_data = tournament_db.get_tournament_data(tournament_id)
    if tournament_data.get("status") == TournamentStatus.CREATED:
        teams_db.unregister_all_teams(tournament_id)
        competition_data = CompetitionAPI.get_competition(platform_id, tournament_data.get("sign_up_competition"))
        if competition_data is not None:
            league_data = LeagueAPI.get_league(platform_id, competition_data.get("competition", {}).get("league_id"))
            team_list = CyanideApi.get_teams(league_data.get("league", {}).get("name"), competition_data.get("competition", {}).get("name"))
            if team_list is not None:
                team_list = team_list.get("teams", [])
                registered_team = []
                for team in team_list:
                    team_model = RequestUtils.get_team(team_id=team.get("id"), platform_id=platform_id, cached_data=True)
                    while team_model is None:
                        team_model = RequestUtils.get_team(team_id=team.get("id"), platform_id=platform_id, cached_data=False)

                    if team_model is not None:
                        reg_data = {
                            "name": team_model.get_name(),
                            "id": team_model.get_id(),
                            "platform_id": platform_id,
                            "logo": team_model.get_logo(),
                            "coach_name": team_model.get_coach().get_name(),
                            "coach_id": team_model.get_coach().get_id(),
                            "race_id": team_model.get_race_id(),
                            "tournament_id": ObjectId(tournament_id),
                            "status": TeamStatus.ACTIVE
                        }
                        user_linked_id = user_db.get_user_linked(team_model.get_coach().get_id(), platform_id)
                        if user_linked_id is not None:
                            user_linked = user_db.get_user(str(user_linked_id))
                            if user_linked.get("naf_name") is not None and user_linked.get("naf_number") is not None:
                                reg_data["naf_name"] = user_linked.get("naf_name")
                                reg_data["naf_number"] = user_linked.get("naf_number")
                        registered_team.append(reg_data)
                teams_db.register_teams(registered_team)
                return {"tournament_id": tournament_id, "teams": len(registered_team)}
            else:
                raise HTTPException(status_code=404, detail="Unable to get list of team")
        else:
            raise HTTPException(status_code=404, detail="SignUp competition not found")
    else:
        raise HTTPException(status_code=500, detail="Invalid tournament status")


def register_team(tournament_id: str, team_model: TeamRegistration):
    user_db = Users()
    team_db = Teams()
    register_data = {
        "name": team_model.name,
        "id": team_model.id,
        "platform_id": team_model.platform_id,
        "logo": team_model.logo,
        "coach_name": team_model.coach_name,
        "coach_id": team_model.coach_id,
        "race_id": team_model.race_id,
        "tournament_id": ObjectId(tournament_id),
        "status": TeamStatus.ACTIVE
    }
    user_linked_id = user_db.get_user_linked(team_model.coach_id, team_model.platform_id)
    if user_linked_id is not None:
        user_linked = user_db.get_user(str(user_linked_id))
        if user_linked.get("naf_name") is not None and user_linked.get("naf_number") is not None:
            register_data["naf_name"] = user_linked.get("naf_name")
            register_data["naf_number"] = user_linked.get("naf_number")
    ret = team_db.register_team(register_data)
    if ret is not None:
        return {"tournament_id": tournament_id, "team": str(ret.inserted_id)}
    raise HTTPException(status_code=500, detail="Unable to register team")


def replace_team(contest_id: str, original_team: str, new_team: str):
    contest_db = Contests()
    contest = contest_db.get_contest(contest_id)
    updated = False
    home_team_id = str(contest["home_team"]["id"])
    if home_team_id == original_team:
        updated = True
        home_team_id = new_team
    away_team_id = str(contest["away_team"]["id"])
    if away_team_id == original_team:
        updated = True
        away_team_id = new_team
    if home_team_id == away_team_id:
        updated = False

    if updated:
        contest_db.update_teams(contest_id, home_team_id, away_team_id)
        return {"contest_id": contest_id}
    else:
        raise HTTPException(status_code=400, detail="Invalid team id.")


def get_standing(tournament_id: str):
    standing_db = Standings()
    standing = standing_db.get_standing(tournament_id)
    teams_db = Teams()
    team_list = teams_db.get_registered_team(tournament_id)
    ret_standing = []

    if standing is not None:
        for team in standing:
            team_data = next((item for item in team_list if team["_id"] == item["_id"]), None)
            if team_data is not None:
                team["team_name"] = team_data["name"]
                team["team_id"] = team_data["id"]
                team["coach_name"] = team_data["coach_name"]
                team["coach_id"] = team_data["coach_id"]
                team["platform_id"] = team_data["platform_id"]
                team["status"] = team_data["status"]
                team["race_id"] = team_data["race_id"]
                team["logo"] = team_data["logo"]
                ret_standing.append(team)
    return {"tournament_id": tournament_id, "standing": ret_standing}


def all_matches_played(tournament_id: str, round_index: int):
    contests_db = Contests()
    contests = contests_db.get_contests(tournament_id, round_index, False)
    match_played = 0
    for contest in contests:
        if contest.get("home_team", {}).get("score") is not None and contest.get("away_team", {}).get("score") is not None:
            match_played += 1
    return match_played == len(contests)


def create_sign_up(tournament_id: str):
    db = Tournaments()
    competition_db = Competitions()
    tournament_data = db.get_tournament_data(tournament_id)
    if tournament_data.get("sign_up") is None:
        if len(tournament_data.get("leagues", [])) > 0:
            username = TMSettings.get_bot_username()
            password = TMSettings.get_bot_password()
            league_id = tournament_data.get("leagues")[0].get("id")
            league_name = tournament_data.get("leagues")[0].get("name")
            csv_data = "SignUp,\r\n"
            filename = "signup.csv"
            csv_file = open(filename, "wt")
            csv_file.write(csv_data)
            csv_file.close()

            command = "./comp_creator --username={} --password={} --leagueIDs=RowId:{}-Server:ONLINE-StoreName:Main --flags=experiencedTeams,resurrection,customTeams --competitionType=ladder --acceptTicket=true {}".format(username, password, league_id, filename)
            os.system(command)
            competitions = CyanideApi.get_competitions(league_name)
            for competition in competitions.get("competitions", []):
                competition_db.add_or_update_competition(competition.get("name"), competition.get("id"), competition.get("league", {}).get("id"), platform_id=1)
                if competition.get("name") == "SignUp" and competition.get("format") == "ladder" and competition.get("league", {}).get("id") == league_id:
                    db.update_tournament(tournament_id, {"sign_up_competition": competition.get("id")})
                    return {"tournament_id": tournament_id, "sign_up_competition": competition}
            raise HTTPException(status_code=500, detail="Error during signup creation")
        else:
            raise HTTPException(status_code=500, detail="No league registered.")
    else:
        raise HTTPException(status_code=500, detail="SignUp already exist.")


def create_competitions(tournament_id: str, round_index: int):
    contest_db = Contests()
    tournament_db = Tournaments()
    team_db = Teams()

    contests = contest_db.get_contests(tournament_id, round_index, False)
    tournament_data = tournament_db.get_tournament_data(tournament_id)
    team_list = team_db.get_registered_team(tournament_id)
    league_required = int(len(contests)/20 + 1)

    if len(tournament_data.get("leagues", [])) < league_required:
        raise HTTPException(status_code=500, detail="Not enough league registered: {}/{}".format(len(tournament_data.get("leagues", [])), league_required))

    def process_creation():
        league_id = tournament_data["leagues"][league_index]["id"]
        filename = "competitions.csv"
        csv_file = open(filename, "wt")
        csv_file.write(csv_data)
        csv_file.close()
        command = "./comp_creator --username={} --password={} --useIDs=True --leagueIDs=RowId:{}-Server:ONLINE-StoreName:Main --turnTimers=3 --flags=experiencedTeams,resurrection,customTeams {}".format(TMSettings.get_bot_username(), TMSettings.get_bot_password(), league_id, filename)
        os.system(command)

    league_index = 0
    csv_data = ""
    for contest in contests:
        home_team_data = next((team for team in team_list if team.get("_id") == contest.get("home_team", {}).get("id")), None)
        away_team_data = next((team for team in team_list if team.get("_id") == contest.get("away_team", {}).get("id")), None)
        csv_data += "Round {} Table {},\r\n".format(contest.get("round_index"), contest.get("table_index"))
        csv_data += "{},RowId:{}-Server:ONLINE-StoreName:Main\r\n".format(home_team_data.get("coach_id"), home_team_data.get("id"))
        csv_data += "{},RowId:{}-Server:ONLINE-StoreName:Main\r\n".format(away_team_data.get("coach_id"), away_team_data.get("id"))

        if contest.get("table_index") % 20 == 0:
            process_creation()
            league_index += 1
            csv_data = ""

    if len(csv_data) > 0:
        process_creation()
