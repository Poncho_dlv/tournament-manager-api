import json
import os


class ApiKey:

    def __init__(self):
        self.json_data = ApiKey.load_json()

    @staticmethod
    def load_json():
        filename = os.environ.get("API_KEY_FILE", "api_keys/api_key.json")
        try:
            with open(filename, encoding="utf-8") as f:
                json_data = json.load(f)
            f.close()
        except FileNotFoundError:
            json_data = {}
        return json_data

    def save_settings(self):
        filename = os.environ.get("API_KEY_FILE", "api_keys/api_key.json")
        with open(filename, 'w') as outfile:
            json.dump(self.json_data, outfile)
        outfile.close()

    def check_or_create_section(self, section):
        if self.json_data.get(section) is None:
            self.json_data[section] = {}

    @staticmethod
    def get_api_keys():
        json_data = ApiKey.load_json()
        return json_data.get("KEYS")

    def set_sync_enabled(self, keys: dict):
        self.json_data["KEYS"] = keys
        self.save_settings()
