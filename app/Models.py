from pydantic import BaseModel
from typing import List

tournament_format = ["single_player", "team_tournament"]
round_format = ["round_robin", "round_swiss"]
platform = ["bb2_pc"]

tournament_format_display_name = {"single_player": "Single player", "team_tournament": "Team tournament"}
round_format_display_name = {"round_robin": "Round robin", "round_swiss": "Swiss"}
platform_display_name = {"bb2_pc": "Blood Bowl 2 Desktop"}


class TournamentCreationModel(BaseModel):
    name: str
    format: str
    platform: str
    round_format: str
    number_of_round: int
    win_value: int
    draw_value: int
    loss_value: int
    tie_breakers: List[str]


class EditContest(BaseModel):
    score_home: int
    score_away: int
    cas_home: int
    cas_away: int
    kill_home: int
    kill_away: int
    date: str
    lock: bool = False
    naf: bool = True


class LeagueRegistration(BaseModel):
    id: int
    name: str


class LeaguesRegistration(BaseModel):
    leagues: List[LeagueRegistration]


class TeamRegistration(BaseModel):
    name: str
    id: int
    platform_id: int
    logo: str
    race_id: int
    coach_name: str
    coach_id: int
