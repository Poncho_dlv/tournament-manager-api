import uvicorn
from fastapi import FastAPI, Header, HTTPException

import Utilities
from ApiKey import ApiKey
from Models import *
from tm_databases.Teams import Teams
from tm_databases.Tournaments import Tournaments, TournamentStatus


def api_key_is_valid(api_key):
    if api_key is None:
        return False
    keys = ApiKey.get_api_keys()
    match = next((item for item in keys if item.get("key") == api_key), None)
    return match is not None


app = FastAPI()


@app.post("/create_tournament", tags=["Tournament"])
async def create_tournament(tournament: TournamentCreationModel, api_key: str = Header(None, alias="API_KEY")):
    if not api_key_is_valid(api_key):
        raise HTTPException(status_code=401, detail="Unauthorized")

    valid, invalid_fields = Utilities.check_tournament_data(tournament)
    if valid:
        db = Tournaments()
        _id = db.register_tournament(tournament)
        return {"tournament_id": str(_id)}
    else:
        return {"invalid_field": invalid_fields, "data": tournament.dict()}


@app.delete("/tournaments/{tournament_id}/delete", tags=["Tournament"])
async def delete_tournament(tournament_id: str, api_key: str = Header(None, alias="API_KEY")):
    if not api_key_is_valid(api_key):
        raise HTTPException(status_code=401, detail="Unauthorized")

    db = Tournaments()
    tournament_data = db.get_tournament_data(tournament_id)
    if tournament_data and tournament_data.get("status") == TournamentStatus.CREATED:
        ret = db.delete_tournament(tournament_id)
        return {"tournament_id": tournament_id, "deleted": ret.deleted_count == 1}
    else:
        return {"invalid_status": tournament_data.get("status"), "tournament_id": tournament_id}


@app.get("/tournaments/active_tournaments", tags=["Tournament"])
async def get_active_tournament(api_key: str = Header(None, alias="API_KEY")):
    if not api_key_is_valid(api_key):
        raise HTTPException(status_code=401, detail="Unauthorized")

    db = Tournaments()
    tournaments = db .get_tournaments()
    for tournament in tournaments:
        tournament["display_name"] = {
            "format": tournament_format_display_name[tournament["format"]],
            "platform": platform_display_name[tournament["platform"]],
            "round_format": round_format_display_name[tournament["round_format"]]
        }
    return tournaments


@app.get("/tournaments/{tournament_id}", tags=["Tournament"])
async def get_tournament(tournament_id: str, api_key: str = Header(None, alias="API_KEY")):
    if not api_key_is_valid(api_key):
        raise HTTPException(status_code=401, detail="Unauthorized")
    db = Tournaments()
    tournament_data = db.get_tournament_data(tournament_id)

    team_db = Teams()
    reg_teams = team_db.get_registered_team(tournament_id)
    nb_teams = len(reg_teams)
    tournament_data["display_name"] = {
        "format": tournament_format_display_name[tournament_data["format"]],
        "platform": platform_display_name[tournament_data["platform"]],
        "round_format": round_format_display_name[tournament_data["round_format"]],
        "registered_teams": nb_teams
    }
    return tournament_data


@app.put("/tournaments/{tournament_id}/start", tags=["Tournament"])
async def start_tournament(tournament_id: str, api_key: str = Header(None, alias="API_KEY")):
    if not api_key_is_valid(api_key):
        raise HTTPException(status_code=401, detail="Unauthorized")
    return Utilities.start_tournament(tournament_id)


@app.put("/tournaments/{tournament_id}/close", tags=["Tournament"])
async def start_tournament(tournament_id: str, api_key: str = Header(None, alias="API_KEY")):
    if not api_key_is_valid(api_key):
        raise HTTPException(status_code=401, detail="Unauthorized")
    return Utilities.close_tournament(tournament_id)


@app.get("/tournaments/{tournament_id}/rounds/{round_index}/contests", tags=["Contest"])
async def get_contests(tournament_id: str, round_index: int,
                       allow_draw: bool = False, public_only: bool = False,
                       api_key: str = Header(None, alias="API_KEY")):
    if not api_key_is_valid(api_key):
        raise HTTPException(status_code=401, detail="Unauthorized")
    return Utilities.get_contests(tournament_id, round_index, allow_draw, public_only)


@app.get("/tournaments/{tournament_id}/current_round", tags=["Contest"])
async def get_current_round(tournament_id: str, api_key: str = Header(None, alias="API_KEY")):
    if not api_key_is_valid(api_key):
        raise HTTPException(status_code=401, detail="Unauthorized")
    return Utilities.get_current_round(tournament_id)


@app.get("/contests/{contest_id}", tags=["Contest"])
async def get_contest(contest_id: str, api_key: str = Header(None, alias="API_KEY")):
    if not api_key_is_valid(api_key):
        raise HTTPException(status_code=401, detail="Unauthorized")
    return Utilities.get_contest(contest_id)


@app.put("/contests/{contest_id}/teams/{team_id}/replace", tags=["Team"])
async def replace_team(contest_id: str, team_id: str, new_team_id: str, api_key: str = Header(None, alias="API_KEY")):
    if not api_key_is_valid(api_key):
        raise HTTPException(status_code=401, detail="Unauthorized")
    return Utilities.replace_team(contest_id, team_id, new_team_id)


@app.put("/contests/{contest_id}/update", tags=["Contest"])
async def update_contest(contest_id: str, contest_data: EditContest, api_key: str = Header(None, alias="API_KEY")):
    if not api_key_is_valid(api_key):
        raise HTTPException(status_code=401, detail="Unauthorized")
    return Utilities.update_contest(contest_id, contest_data)


@app.delete("/tournaments/{tournament_id}/rounds/{round_index}/delete", tags=["Contest"])
async def delete_round(tournament_id: str, round_index: int, api_key: str = Header(None, alias="API_KEY")):
    if not api_key_is_valid(api_key):
        raise HTTPException(status_code=401, detail="Unauthorized")
    return Utilities.delete_round(tournament_id, round_index)


@app.put("/tournaments/{tournament_id}/rounds/{round_index}/publish", tags=["Contest"])
async def publish_round(tournament_id: str, round_index: int, api_key: str = Header(None, alias="API_KEY")):
    if not api_key_is_valid(api_key):
        raise HTTPException(status_code=401, detail="Unauthorized")
    return Utilities.publish_round(tournament_id, round_index)


@app.put("/tournaments/{tournament_id}/rounds/{round_index}/unpublish", tags=["Contest"])
async def unpublish_round(tournament_id: str, round_index: int, api_key: str = Header(None, alias="API_KEY")):
    if not api_key_is_valid(api_key):
        raise HTTPException(status_code=401, detail="Unauthorized")
    return Utilities.unpublish_round(tournament_id, round_index)


@app.post("/tournaments/{tournament_id}/register_teams", tags=["Team"])
async def register_teams(tournament_id: str, api_key: str = Header(None, alias="API_KEY")):
    if not api_key_is_valid(api_key):
        raise HTTPException(status_code=401, detail="Unauthorized")
    return Utilities.register_teams(tournament_id)


@app.post("/tournaments/{tournament_id}/register_team", tags=["Team"])
async def register_team(tournament_id: str, team_model: TeamRegistration, api_key: str = Header(None, alias="API_KEY")):
    if not api_key_is_valid(api_key):
        raise HTTPException(status_code=401, detail="Unauthorized")
    return Utilities.register_team(tournament_id, team_model)


@app.put("/teams/{team_id}/unregister", tags=["Team"])
async def unregister_team(team_id: str, api_key: str = Header(None, alias="API_KEY")):
    if not api_key_is_valid(api_key):
        raise HTTPException(status_code=401, detail="Unauthorized")
    db = Teams()
    success = db.disable_registered_team(team_id)
    return {"team_id": team_id, "success": success}


@app.get("/tournaments/{tournament_id}/teams", tags=["Team"])
async def get_registered_team(tournament_id: str, active_only: bool = True, api_key: str = Header(None, alias="API_KEY")):
    if not api_key_is_valid(api_key):
        raise HTTPException(status_code=401, detail="Unauthorized")
    db = Teams()
    teams = db.get_registered_team(tournament_id, active_only)
    return {"teams": teams}


@app.get("/teams/{team_id}", tags=["Team"])
async def unregister_team(team_id: str, api_key: str = Header(None, alias="API_KEY")):
    if not api_key_is_valid(api_key):
        raise HTTPException(status_code=401, detail="Unauthorized")
    db = Teams()
    team_data = db.get_team_data(team_id)
    if team_data is not None:
        team_data["_id"] = str(team_data["_id"])
        team_data["tournament_id"] = str(team_data["tournament_id"])
    return team_data


@app.put("/tournaments/{tournament_id}/update_leagues", tags=["Tournament"])
async def register_league(tournament_id: str, leagues: LeaguesRegistration, api_key: str = Header(None, alias="API_KEY")):
    if not api_key_is_valid(api_key):
        raise HTTPException(status_code=401, detail="Unauthorized")
    db = Tournaments()
    success = db.update_tournament(tournament_id, leagues.dict())
    return {"tournament_id": tournament_id, "success": success}


@app.post("/tournaments/{tournament_id}/create_sign_up", tags=["Tournament"])
async def create_sign_up(tournament_id: str, api_key: str = Header(None, alias="API_KEY")):
    if not api_key_is_valid(api_key):
        raise HTTPException(status_code=401, detail="Unauthorized")
    return Utilities.create_sign_up(tournament_id)


@app.post("/tournaments/{tournament_id}/rounds/{round_index}/create_competitions", tags=["Tournament"])
async def create_competitions(tournament_id: str, round_index: int,  api_key: str = Header(None, alias="API_KEY")):
    if not api_key_is_valid(api_key):
        raise HTTPException(status_code=401, detail="Unauthorized")
    return Utilities.create_competitions(tournament_id, round_index)


@app.get("/tournaments/{tournament_id}/standing", tags=["Tournament"])
async def get_standing(tournament_id: str, api_key: str = Header(None, alias="API_KEY")):
    if not api_key_is_valid(api_key):
        raise HTTPException(status_code=401, detail="Unauthorized")
    return Utilities.get_standing(tournament_id)

if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)
