FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7

COPY ./app /app

# Set the working directory
WORKDIR /app

# Install the dependencies
RUN pip install -r requirements.txt